" Vim vimrc
" License: ISC
" Maintainer: Steve
" Last Change: 2020 Apr 10

" Normal mode commands for file type .vim:
" zM - folds all markers
" zR - unfolds all markers
" za - while inside maker, folds/unfolds curren marker

" :::::::::::::::::::
" :: USER SETTINGS ::
" :::::::::::::::::::

" allow features not present in vi
set nocompatible

" clear previous autocmd commands
autocmd!

" milliseconds to wait for next key in a sequence
set ttimeoutlen=50

" set spacebar to be the leader key in normal mode
noremap <Space> <Nop>
let mapleader = "\<Space>"

" the terminal has 256 colors
set t_Co=256

" light colorscheme that's easy on eyes {{{
colorscheme steves

if &diff
  colorscheme steves
endif
" }}}

" allow switching beetween buffers without saving first
set hidden

" set undo, swap, backup directories {{{
let s:my_home_dir = expand('$HOME')
if has('win32')
  if !isdirectory(s:my_home_dir . '\vimfiles\undodir')
    call mkdir(s:my_home_dir . '\vimfiles\undodir', "p")
  endif
  let &undofile = 1
  let &undodir = s:my_home_dir . '\vimfiles\undodir//'

  if !isdirectory(s:my_home_dir . '\vimfiles\swap')
    call mkdir(s:my_home_dir . '\vimfiles\swap', "p")
  endif
  let &directory = s:my_home_dir . '\vimfiles\swap//'

  if !isdirectory(s:my_home_dir . '\vimfiles\backupdir')
    call mkdir(s:my_home_dir . '\vimfiles\backupdir', "p")
  endif
  let &backupdir = s:my_home_dir . '\vimfiles\backupdir//'
else
  if !isdirectory(s:my_home_dir . '/.vim/undodir')
    call mkdir(s:my_home_dir . '/.vim/undodir', "p")
  endif
  let &undofile = 1
  let &undodir = s:my_home_dir . '/.vim/undodir//'

  if !isdirectory(s:my_home_dir . '/.vim/swap')
    call mkdir(s:my_home_dir . '/.vim/swap', "p")
  endif
  let &directory = s:my_home_dir . '/.vim/swap//'

  if !isdirectory(s:my_home_dir . '/.vim/backupdir')
    call mkdir(s:my_home_dir . '/.vim/backupdir', "p")
  endif
  let &backupdir = s:my_home_dir . '/.vim/backupdir//'
endif
" }}}

" Save backups of files in backupdir
set writebackup
set backup
" Make vim work with the 'crontab -e' command
set backupskip+=/var/spool/cron/*
" Skip backup for 'netrwhist' file
let &backupskip = &backupskip . ',' . expand('~/.vim/.netrwhist')
" Use swapfile
set swapfile
" Use undofile
set undofile

" In Terminal position the cursor, Visually select and scroll with the mouse. {{{
" Only xterm, (and st) can grab the mouse events when using the shift key, for other terminals use ':', select text and press Esc.
if has('mouse')
  if &term =~? '\v[xs]t.*'
    set mouse=a
  else
    set mouse=nvi
  endif
endif
" }}}

" language and font settings {{{
set encoding=utf-8
" To make vim understand UTF-16 LE and such; utf-16 is UTF-16 BE
" If working with BOMless UTF 16 files, move utf-16le or utf-16 before ucs-bom
" set fileencodings=ucs-bom,utf-16le,utf-16,utf-8,default,latin1,cp1251

" Instead of langmap, use 'ruscmd' plugin
" set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz

" english Menu and UI
set langmenu=en_US.UTF-8
language en_US.UTF-8

if has("gui_running")
  " set guifont=DejaVu_Sans_Mono:h12:cRUSSIAN:qDRAFT
  " set guifont=Liberation_Mono:h12:cRUSSIAN:qDRAFT
  " set guifont=Monospace\ 16
  set guifont=Liberation\ Mono\ 16
  " remove menu and scrollbars from gui
  set guioptions-=mrL
  " gvim window size
  set lines=30
  set columns=85
  if has('win32')
    " remove tearoff menu in windows gui
    set guioptions-=t
  endif
endif
" }}}

" syntax
filetype plugin indent on
syntax on

" matchit makes % normal command work better
packadd! matchit

" indentation and spacing {{{
" remember indentation for new line
set autoindent
" replace tabs with spaces
set expandtab
" 1 tab == 2 spaces
set tabstop=2
" 2 spaces for autoindent
set shiftwidth=2
" delete 2 spaces in indent with single backspace
set softtabstop=2
" the lenght of strings in letters
" set textwidth=132
set textwidth=140
" }}}

" fix backspace on most terminals
set backspace=indent,eol,start

" quality-of-life improvements to Vim: {{{
" disable bell
set belloff=all
" verbose cscope output
set cscopeverbose
" C-N in insert mode for autocompletion
set complete-=i
" commands history
set history=500
" C-A, C-X to increment/decrement numbers
set nrformats-=octal
" do not carry options between sessions
set sessionoptions-=options
" avoid Hit-Enter prompts
set shortmess=atI
" fast tty connection
set ttyfast
" do not redraw screen when using macros, registers, etc.
" tremendously increase performance of macro commands
set lazyredraw
" }}}

" load all plugins
" packloadall
" load help files for all plugins
" silent! helptags ALL

" folding options {{{
" fold based on indentation in code
set foldmethod=indent
set foldignore=
" visualize folds
set foldcolumn=1
" unfold markers after opening a file:
autocmd BufRead * normal! zR
" }}}

" when opening a document, return to the last cursor position
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") |
  \   execute "normal! g`\"" |
  \ endif

" don't write swapfile on most commonly used directories for NFS mounts or USB sticks
autocmd BufNewFile,BufReadPre /media/*,/run/media/*,/mnt/* set directory=~/tmp,/var/tmp,/tmp

" enable enhanced tab autocompletion
set wildmenu
set wildmode=list:longest,full

" line numbers
set number
set relativenumber

" search settings
set hlsearch
set incsearch
" set ignorecase

" copy into system registers
" set clipboard=unnamed,unnamedplus

" look for tags in parent directories
" set tags=tags;

" info at the bottom
set ruler
" pretty word-wrap
set linebreak
" pretty long lines
set display+=lastline

" Custom commands ------------------------------ {{{

" Modified function from gvim default vimrc
" !!! only works in Windows !!!
" function! MyDiff() abort
"   let l:optn = '-a --binary '
"   if &diffopt =~? 'icase'
"     let l:optn = l:optn . '-i '
"   endif
"   if &diffopt =~? 'iwhite'
"     let l:optn = l:optn . '-b '
"   endif
"   let l:arg1 = v:fname_in
"   if l:arg1 =~? ' ' | let l:arg1 = '"' . l:arg1 . '"' | endif
"   let l:arg1 = substitute(l:arg1, '!', '\!', 'g')
"   let l:arg2 = v:fname_new
"   if l:arg2 =~? ' ' | let l:arg2 = '"' . l:arg2 . '"' | endif
"   let l:arg2 = substitute(l:arg2, '!', '\!', 'g')
"   let l:arg3 = v:fname_out
"   if l:arg3 =~? ' ' | let l:arg3 = '"' . l:arg3 . '"' | endif
"   let l:arg3 = substitute(l:arg3, '!', '\!', 'g')
"   if $VIMRUNTIME =~? ' '
"     if &sh =~? '\<cmd'
"       if empty(&shellxquote)
"         let l:shxq_sav = ''
"         set shellxquote&
"       endif
"       let cmd = '"' . $VIMRUNTIME . '\diff"'
"     else
"       let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
"     endif
"   else
"     let cmd = $VIMRUNTIME . '\diff'
"   endif
"   let cmd = substitute(cmd, '!', '\!', 'g')
"   silent execute '!' . cmd . ' ' . l:optn . l:arg1 . ' ' . l:arg2 . ' > ' . l:arg3
"   if exists('l:shxq_sav')
"     let &shellxquote=l:shxq_sav
"   endif
" endfunction
" 
" set diffexpr=MyDiff()

" Function to load the template for current file type, if it exists.
function! s:MyLoadVimTemplate() abort
  let l:templateFile = expand("~/Additional/templatesVim/") . &filetype . ".txt"
  if filereadable(l:templateFile)
    execute "0r " . l:templateFile
    execute "normal! zR"
  endif
endfunction

nnoremap <silent> <Leader>tt :<C-U>call <SID>MyLoadVimTemplate()<C-M>

" Difference between current buffer and original file
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis | wincmd p | diffthis
endif
" }}}

" C file settings ------------------------------ {{{
augroup filetype_c
  autocmd!
  autocmd FileType c setlocal foldignore=#
augroup END
" }}}

" Python file settings ------------------------- {{{
augroup filetype_python
  autocmd!
  autocmd FileType python setlocal tabstop=4 shiftwidth=4 softtabstop=4
augroup END
" }}}

" Vimscript file settings ---------------------- {{{
augroup filetype_vim
  autocmd!
  autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}

" remaps {{{
" repeat the last substitute with options
nnoremap & :&&<C-M>
xnoremap & :&&<C-M>

" C18 multi-line comments
nnoremap <silent> <Leader>m a/*  */<Left><Left><Left>

" Search visually selected text with * #
function! s:VisSrch(cmdtype) abort
  let l:temp = @s
  normal! gv"sy
  let @/ = '\V' . substitute(escape(@s, a:cmdtype.'\'), '\n', '\\n', 'g')
  let @s = l:temp
endfunction

xnoremap <silent> * :<C-U>call <SID>VisSrch('/')<C-M>/<C-R>=@/<C-M><C-M>
xnoremap <silent> # :<C-U>call <SID>VisSrch('?')<C-M>?<C-R>=@/<C-M><C-M>

" traverse Vim's lists
nnoremap <silent> [b :bprevious<C-M>
nnoremap <silent> ]b :bnext<C-M>
nnoremap <silent> [B :bfirst<C-M>
nnoremap <silent> ]B :blast<C-M>

" show whitespace characters
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<,nbsp:+
nnoremap <silent> <F5> :set list!<C-M>
inoremap <silent> <F5> <C-O>:set list!<C-M>
cnoremap <silent> <F5> <C-C>:set list!<C-M>

" open/close Lex file explorer
nnoremap <silent> <F6> :<C-U>Lex<C-M>2j:vertical resize 18<C-M>
inoremap <silent> <F6> <C-[>:<C-U>Lex<C-M>2j:vertical resize 18<C-M>
cnoremap <silent> <F6> <C-[>:<C-U>Lex<C-M>2j:vertical resize 18<C-M>

" toggle highlighting of color column
function! s:CColToggle() abort
  if &colorcolumn
    let &colorcolumn = 0
  else
    let &colorcolumn = 80
  endif
endfunction
nnoremap <silent> <F7> :<C-U>call <SID>CColToggle()<C-M>
inoremap <silent> <F7> <C-O>:<C-U>call <SID>CColToggle()<C-M>
cnoremap <silent> <F7> <C-O>:<C-U>call <SID>CColToggle()<C-M>

" toggle highlighting of cursor line
function! s:ClToggle() abort
  if &cursorline
    let &cursorline = 0
  else
    let &cursorline = 1
  endif
endfunction
nnoremap <silent> <Leader>h :<C-U>call <SID>ClToggle()<C-M>

" reverse highlighted text
vnoremap <Leader>r c<C-O>:set revins<C-M><C-R>"<C-[>:set norevins<C-M>
" }}}

" insert mode remaps: {{{

" Alt-Space to move one letter to the right in insert mode
inoremap <silent> <M-Space> <C-[>la
" Ctrl-D-Ctrl-D to insert current date
inoremap <silent> <C-D><C-D> <C-R>=strftime("%d %b %Y")<C-M>
" Ctrl-T-Ctrl-T to insert current time
inoremap <silent> <C-T><C-T> <C-R>=strftime("%H:%M")<C-M>
" Ctrl-D-Ctrl-T to insert current date and time
inoremap <silent> <C-D><C-T> <C-R>=strftime("%Y-%m-%d-%T")<C-M>
" Be able to undo Ctrl-U in insert mode
inoremap <silent> <C-U> <C-G>u<C-U>
" }}}

" Set powershell as default shell (Windows only)
" set shell=powershell
" set shellcmdflag=-command

